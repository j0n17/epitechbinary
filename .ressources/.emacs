(load "std.el")
(load "std_comment.el")
(menu-bar-mode -1)
(setq-default show-trailing-whitespace t)
(add-hook 'c-mode-hook '(lambda () (highlight-lines-matching-regexp ".\\{81\\}" 'hi-yellow)))

(delete-selection-mode 1)