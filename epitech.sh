#!/bin/bash

echo "Script d'installation Epitech"
echo "-----------------------------"
echo " "
echo " "
echo "Ce script va installer :"
echo " - rendu"
echo " - le header emacs"
echo " - quelques alias"

if [ -s /usr/bin/git ]; 
then
    echo "Ajout du script rendu"
    chmod +x .ressources/rendu*
    chmod +x .ressources/blih*
    cp .ressources/rendu* /usr/bin/.
    cp .ressources/blih* /usr/bin/.
    echo "merci de taper les deux commandes suivantes :
'ssh-keygen -t rsa -f ~/.ssh/id_rsa_blih'
'blih.py -u login_x sshkey upload ~/.ssh/id_rsa_blih.pub'"

else
    sudo apt-get install git python3.3
    sudo apt-get instal python3.2
fi

if [ ! -s /usr/bin/QNetSoul ];
then
    chmod +x .ressources/QNetSoul
    chmod +x .ressources/Updater
    cp .ressources/QNetSoul /usr/bin/.
    cp .ressources/Updater /usr/bin/.
fi

if [ -s /usr/bin/emacs ];
then
    cp .ressources/std* /usr/share/emacs/site-lisp/.
    cp .ressources/.emacs ~/.
fi

if [ -s ~/.bashrc ]; 
then
    echo " Ajout de l'alias ne dans le bashrc"
    echo "
    alias ns='QNetSoul & exit'
    alias ne='emacs -nw'
    alias ls='ls --color=auto'
    alias l='ls -la'
    alias ll='ls -l'
    alias la='ls -a'
    alias rmtmp='rm *~'
    alias rmtmpp='rm .*~'
" >> ~/.bashrc

    echo "Fait !"
fi
